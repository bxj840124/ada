#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <utility>

using namespace std; 

long long answer;

vector<pair<long long, long long> > merge(int left, int right, vector<pair<long long, long long> > l, vector<pair<long long, long long> > r)
{
        int i, j;
        int mid = (left + right) / 2;
        vector<pair<long long, long long> > buffer;
        
        // Combine.
        i = 0;
        j = 0;
        while (i < mid - left + 1 && j < right - mid)
        {
                if (l[i].second > r[j].second)          // Suitable.
                {
                        buffer.push_back(l[i]);
                        answer += (long long)(right - mid - j);
                        i++;
                }
                else if (l[i].second < r[j].second)             // Unsuitable.
                {
                        buffer.push_back(r[j]);
                        j++; 
                }
                else          // Find duplicate numbers.
                {
                        if (l[i].first == r[j].first)		// Two pairs of numbers are totally same.
                        {
                                long long side1, side2;
                                int m, n;
                                side1 = l[i].first;
                                side2 = l[i].second;
                                for (m = 0; i < mid - left + 1 && (l[i].first == side1 && l[i].second == side2); m++, i++)
                                {
                                        buffer.push_back(l[i]);
                                }
                                for (n = 0; j < right - mid && (r[j].first == side1 && r[j].second == side2); n++, j++)
                                {
                                        buffer.push_back(r[j]);
                                }
                                answer += (long long)(2 * m) * n + (long long)(m * (right - mid - j));
                        }
                        else 		// Only the long side is identical.
                        {
                                answer += (long long)(right - mid - j);
                                buffer.push_back(l[i]);
                                i++;
                        }
                }
        }
        for (; i < mid - left + 1; i++)		// Store the rest elements if while loop ended halfway.
                buffer.push_back(l[i]);
        for (; j < right - mid; j++)
                buffer.push_back(r[j]);
        return buffer;
}


vector<pair<long long, long long> > mergeSort(int left, int right, vector<pair<long long, long long> > s)
{
        int mid = (left + right) / 2;
        int num = right - left + 1;
        int i, j;
        vector<pair<long long, long long> > tmp1;
        vector<pair<long long, long long> > tmp2;
        vector<pair<long long, long long> > ltmp;
        vector<pair<long long, long long> > rtmp;

        if (num == 1) return s;		// Terminate condition.
        else		// Divide the range into two pieces.
        {
                for (i = 0; i < mid - left + 1; i++)
                {
                        tmp1.push_back(s[i]);
                }
                for (; i < num; i++)
                {
                        tmp2.push_back(s[i]);
                }
                ltmp = mergeSort(left, mid, tmp1);		// Recurence.
                rtmp = mergeSort(mid + 1, right, tmp2);
                return merge(left, right, ltmp, rtmp);
        }
}

bool sideSort(pair<long long, long long> a, pair<long long, long long> b)
{
        // Put the long side at first, and short side at second.
        // All sides are sorted in descending order.
        if (a.first == b.first) return a.second > b.second;
        else return a.first > b.first; 
}

int main(void)
{
        int T, n;               // T = number of test cases, n = number of boxes.
        int i, j;
        pair<long long, long long> input;
        vector<pair<long long, long long> > storage;
        long long tmp;

        scanf("%d", &T);
        for (int i = 0; i < T; i++) 
        {
                scanf("%d", &n);
                answer = (long long int)0;
                for (int j = 0; j < n; j++) 
                {
                        scanf("%lld %lld", &input.first, &input.second);
                        if (input.first < input.second) 		// Swap the int pair, put the larger number at the front,
                        {
                                tmp = input.first;
                                input.first = input.second;
                                input.second = tmp;
                        }
                        storage.push_back(input);
                }
                sort(storage.begin(), storage.end(), sideSort);		// Sort according to the long side.
                mergeSort(0, n - 1, storage);
                printf("%lld\n", answer);
                storage.clear();
        }
        return 0;
} 