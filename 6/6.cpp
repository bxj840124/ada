#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int n, k;	// n: number of cities; k: number of airports.
vector<int> city;	// Positions of cities.
int IntervalNumber(int max_interval_width)
{
	int airport = 1, start = 1, count = 0;
	int i = 1;
	while (i <= n)	// Check if the distance is out of bound.
	{
		if (city[i] - city[start] > max_interval_width)
		{
			start = i - 1;
			count++;
			if (count == 2)	// After first out-of-bound set an airport, after the second one set a new interval.
			{
				airport++;
				start = i;
				count = 0;
			}
			else	continue;
			if (airport > k)	break;	// Check if set too many airports.
		}
		i++;
	}
	return airport;
}

void BinarySearch(int left, int right)
{
	int ans = right;

	for (int mid = (left + right) / 2; left <= right; mid = (left + right) / 2)	// Use binary search to find ideal distance.
	{
		int p = IntervalNumber(mid);
		if (p <= k)
		{
			right = mid - 1;
			ans = mid;
		}
		else 
		{
			left = mid + 1;
		}
	}
	printf("%d\n", ans);
}

int main(void)
{
	int T;	// T: number of test cases.
	int c;	// Input city.

	scanf("%d", &T);
	for (int i = 0; i < T; i++)
	{
		city.push_back(0);
		scanf("%d", &n);
		scanf("%d", &k);
		for (int j = 1; j <= n; j++)
		{
			scanf("%d", &c);
			city.push_back(c);
		}
		sort(city.begin(), city.end());
		BinarySearch(0, city[n]);
		city.clear();
	}
	return 0;
}