#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

struct Node
{
	int connect_node;
	int weight;
};

vector<vector<Node> > V(100001);	// Stores nodes.
vector<int> mark(100001, 0);	// Record the nodes.
vector<int> tmp(100001, 0);	// Use to work.
Node node;
int n, root, counter;

void Visit(int vertex, int w)	// Traversal.
{
	counter++;
	tmp[vertex] = 0;
	for (int i = 0; i < V[vertex].size(); i++)
	{
		if ((tmp[V[vertex][i].connect_node] == 1) && ((V[vertex][i].weight | w) <= w))
		{
			Visit(V[vertex][i].connect_node, w);
		}
	}
}

int BinarySearch(int left, int right)
{
	for (int mid = (left + right) / 2; left < right; mid = (left + right) / 2)
	{
		for (int i = 0; i < 100001; i++)	// Reset.
		{
			tmp[i] = mark[i];
		}
		counter = 0;
		Visit(root, mid);	// Insert root.
		if (counter == n)	// All nodes are visited.
		{
			right = mid;
		}
		else
		{
			left = mid + 1;
		}
	}
	return right;
}

int main(void)
{
	int T;	// Number of test cases.
	int m;	// Numbers of nodes and edges in G.
	int a, b, c;
	int bound = 1;
	int max_weight = 0;
	int ans;

	scanf("%d", &T);	// Input number of test cases.
	while (T--)
	{
		scanf("%d %d", &n, &m);	//	Input number of nodes and edges.
		while (m--)
		{	
			scanf("%d %d %d", &a, &b, &c);	// Input edges.
			node.weight = c;
			if (c > max_weight)
			{
				max_weight = c;
			}
			mark[a] = 1;
			mark[b] = 1;
			node.connect_node = b;
			V[a].push_back(node);
			node.connect_node = a;
			V[b].push_back(node);
		}
		root = a;	// Set root.
		for (int i = 0; i <= 32; i++)	// Find BinarySearch bound.
		{
			if (max_weight <= bound)
			{
				max_weight = max_weight | bound;
				break;
			}
			bound = (bound << 1) + 1;
		}
		ans = BinarySearch(0, max_weight);
		printf("%d\n", ans);
		for (int i = 0; i < 100001; i++)	// Clear
		{
			V[i].clear();
		}
		mark.clear();
	}
	return 0;
}