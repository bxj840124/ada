#include <iostream>
#include <cstdio>

int main()
{
	int i = 0, n;
	long long A, B;

	scanf("%d", &n);
	while (i < n) {
		scanf("%lld %lld", &A, &B);
		printf("%lld\n", A + B);
		i++;
	}
	return 0;
}
