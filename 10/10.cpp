#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;

int main(void)
{
	int T;	// Number of test cases.
	int n;	// Number of SmallxKuos.
	int i, j;

	scanf("%d", &T);
	while (T--)
	{
		int max_intelligence = -1;
		scanf("%d", &n);
		int intelligence[n + 1];
		int answer[n + 1];
		for (i = 1; i <= n; i++)
		{
			scanf("%d", &intelligence[i]);
			if (intelligence[i] > max_intelligence)
			{
				max_intelligence = intelligence[i];
			}
			answer[i] = -1;
		}

		for (i = 1; i <= n; i++)
		{
			j = i - 1;
			while (j != i)
			{
				if (intelligence[i] == max_intelligence)
				{
					answer[i] = 0;
					break;
				}
				else
				{
					if (j == 0)
					{
						j = n;
					}
					if ((intelligence[i] == intelligence[j]) && (answer[j] != -1))
					{
						answer[i] = answer[j];
						break;
					}
					else if (intelligence[i] < intelligence[j])
					{
						answer[i] = j;
						break;
					}
					else if (answer[j] != -1)
					{
						j = answer[j];
					}
					else
					{
						j--;
					}
				}
			}
		}
		for (i = 1; i <= n; i++)
		{
			if (i == n)
			{
				printf("%d\n", answer[i]);
			}
			else
			{
				printf("%d ", answer[i]);
			}
		}
	}

	return 0;
}