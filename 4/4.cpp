#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
using namespace std;

#define MAX 2014
int table[MAX][MAX];		// DP table.
void print_LCS(char* c1, int i, int j, char lable[MAX][MAX], char lcs_head[MAX][MAX])		// Print LCS.
{
    // Stops when the first or second sequence be emtpy.
    if (i == 0 || j == 0)
    {
        	return;
    } 
    if (i > 0 && j > 0)
    {
 		if (lable[i][j] == 'D')		// Move left-up.
    	{
    		printf("%c", c1[i - 1]);
       	 	print_LCS(c1, i - 1, j - 1, lable, lcs_head);
    	}
        else if (lcs_head[i][j] == c1[i - 1])
        {
            print_LCS(c1, i, j - 1, lable, lcs_head);
        }
        else
        {
            print_LCS(c1, i - 1, j, lable, lcs_head);
        }
        /*else if (lable[i][j] == 'U')		// Move up.
   	 	{
       	 	print_LCS(c1, i - 1, j, lable);
    	}
    	else if (lable[i][j] == 'L')		// Move left.
 		{
       	 	print_LCS(c1, i, j - 1, lable);		
 		}*/
 	}
}

void LCS(char* c1, char* c2)		// Build three tables.
{
	int c1_length = strlen(c1);
	int c2_length = strlen(c2);
	char lcs_head[MAX][MAX];		// Record the head character of current LCS.
	char lable[MAX][MAX];		// Record the trace_back direction.
	
	for (int i = 0; i <= c1_length; i++)		// Set table to be 0.
	{
		for (int j = 0; j <= c2_length; j++)
		{
			table[i][j] = 0;
		}
	}
    for (int i = 1; i <= c1_length; i++)
    {
        for (int j = 1; j <= c2_length; j++)
        {
            if (c1[i - 1] == c2[j - 1])		// If the current character belongs to LCS, move left-up.
            {
                table[i][j] = table[i - 1][j - 1] + 1;      // the length of e1 is 1.
                lable[i][j] = 'D';
                lcs_head[i][j] = c1[i - 1];                
            }
            else if (c1[i - 1] != c2[j - 1])
            {
                if (table[i - 1][j] > table[i][j - 1])		// Choose the longer one, move up.
                {
                    table[i][j] = table[i - 1][j];
                    lable[i][j] = 'U';
                    lcs_head[i][j] = lcs_head[i - 1][j];
                }
                else if (table[i - 1][j] < table[i][j - 1])		// Choose the longer one, ove left.
                {
                    table[i][j] = table[i][j - 1];
                    lable[i][j] = 'L';
                    lcs_head[i][j] = lcs_head[i][j - 1];
                } 
                else if (table[i - 1][j] == table[i][j - 1])	// The two cases are identicle.
                {
            	   if (lcs_head[i - 1][j] > lcs_head[i][j - 1])		// Choose the lexicographically smaller one.
            	   {
            		  table[i][j] = table[i][j - 1];
            		  lable[i][j] = 'L';
            		  lcs_head[i][j] = lcs_head[i][j - 1];
            	   }
            	   else
            	   {
            		  table[i][j] = table[i - 1][j];
            		  lable[i][j] = 'U';
            		  lcs_head[i][j] = lcs_head[i - 1][j];
            	   }
                }
            }
        }
    }
    print_LCS(c1, c1_length, c2_length, lable, lcs_head);
}

void reverse(char* str)		// Reverse function.
{
    int i, j;
    char temp[MAX];
    for (i = strlen(str) - 1, j = 0; i + 1 != 0; --i, ++j)
    {
        temp[j] = str[i];
    }
    temp[j] = '\0';
    strcpy(str, temp);
}

int main(void)
{
	int T;		// The number of test cases.
	scanf("%d", &T);
	for (int i = 0; i < T; i++)
	{
		char c1[MAX], c2[MAX];		// Input two strings.
		scanf("%s", c1);
		scanf("%s", c2);
		reverse(c1);		// Reverse the sequence of the string.
		reverse(c2);		// Reverse the sequence of the string.
		LCS(c1, c2);
		printf("\n");
	}
	return 0;
}
