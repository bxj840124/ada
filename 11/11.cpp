#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <climits>
using namespace std;

__uint128_t adj[128];
__uint128_t one = 1;
__uint128_t zero = 0;
int max_num = 0;

void backtrack(int R ,__uint128_t P, __uint128_t X)
{
	if (P == 0 && X == 0) // P and X are both empty.
	{
		if (R > max_num)	// Check if it is maximum clique.
		{
			max_num = R;
		}
		return;
	}

	int p;	// Pivot. 
	__uint128_t Q;	// P \ N(u).

	if (((P | X) & ((one << 50) - 1)) == zero)
	{
		p = __builtin_ctzll((P | X) >> 50) + 50;
	}
	else 
	{
		p = __builtin_ctzll(P | X);
	}
	Q = P & ~adj[p];
	int possible = __builtin_popcountll(P & ((one << 50) - 1)) + __builtin_popcountll(P >> 50) + R;

	while (Q && possible > max_num)	// For each vertex in P \ N(u).
	{
		int i;
		if ((Q & ((one << 50) - 1)) == zero)
		{
			i = __builtin_ctzll(Q >> 50) + 50;
		}
		else 
		{
			i = __builtin_ctzll(Q);
		}
		if (Q & adj[i] || i == p)
		{
			backtrack(R + 1, P & adj[i], X & adj[i]);	// Recursion (R ⋃ {v}, P ⋂ N(v), X ⋂ N(v)).
		}
    	Q &= ~(one << i);	// Delete current vertex.
    	P &= ~(one << i);	// P \ {v}.
    	X |= (one << i);	// X ⋃ {v}.
    	possible--;
	}
}

int main(void)
{
	int T;	// Number of test cases.
	int n, m;	// Number of vertices and edges.

	scanf("%d", &T);
	while (T--)
	{
		int a, b;
		scanf("%d%d", &n, &m);	// Input vertices and edges.
		for (int i = 0; i < n; i++)	// Build complete graph.
		{
			adj[i] = (one << n) - 1;
			adj[i] &= ~(one << i);
		}
		for (int i = 0; i < m; i++)	// build complement graph of input.
		{
			scanf("%d%d", &a, &b);
			adj[a] &= ~(one << b);
			adj[b] &= ~(one << a);
		}
		backtrack(0, (one << n) - 1, 0);
		printf("%d\n", max_num);	// Print the number of elements in maximum clique.
		max_num = 0;
	}
	return 0;
}