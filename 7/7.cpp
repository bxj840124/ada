#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int money[10] = {1, 5, 10, 20, 50, 100, 200, 500, 1000, 2000};	// Coins and banknotes Small2Kuo has.
int money_num[10]; // Numbers of coins and banknotes Small2Kuo has.
int total_money_num;	// Total amount of coins and banknotes.

void beverage(int p)
{
	int answer;
	int p1 = p, p2 = p, p3 = p, p4 = p;	// Store the original target.
	int count1 = 0;	// count1: odd $50 & odd &500.
	int count2 = 0;	// count2: odd $50 & even $500.
	int count3 = 0;	// count3: even $50 & odd $500.
	int count4 = 0; // count4: even $50 & even $500.
	
	int change500_oddnum = (money_num[7] - 1) / 2;
	int change500_evennum = money_num[7] / 2;
	int change50_oddnum = (money_num[4] - 1) / 2;
	int change50_evennum = money_num[4] / 2;

	if (p1 >= 550 && money_num[7] != 0 && money_num[4] != 0)
	{
		p1 -= 550;
		count1 += 2;
	}
	else if (p1 >= 500 && money_num[7] != 0)
	{
		p1 -= 500;
		count1 += 1;
	}
	else if (p1 >= 50 && money_num[4] != 0)
	{
		p1 -= 50;
		count1 += 1;
	}

	if (p2 >= 50 && money_num[4] != 0)
	{
		p2 -= 50;
		count2 += 1;
	}

	if (p3 >= 500 && money_num[7] != 0)
	{
		p3 -= 500;
		count3 += 1;
	}

	// Odd $50 and odd $500 case.
	for (int i = 9; i >= 0; i--) 
	{
		if (money_num[i] == 0)
			continue;
		else if (i == 7 && p1 >= 500)	// Dealing with special case: $500.
		{
			if (p1 >= 1000 * change500_oddnum)
			{
				count1 += change500_oddnum * 2;
				p1 -= 1000 * change500_oddnum;
			}
			else
			{
				count1 += (p1 / 1000) * 2;
				p1 %= 1000;
			}	
		}
		else if (i == 4 && p1 >= 50)	// Dealing with special case: $50.
		{
			if (p1 >= 100 * change50_oddnum)
			{
				count1 += change50_oddnum * 2;
				p1 -= 100 * change50_oddnum;
			}
			else
			{
				count1 += (p1 / 100) * 2;
				p1 %= 100;
			}
		}
		else
		{
			if (p1 >= money[i] * money_num[i])
			{
				count1 += money_num[i];
				p1 -= money[i] * money_num[i];
			}
			else
			{
				count1 += p1 / money[i];
				p1 %= money[i];
			}	
		}
		if (p1 == 0)
			break;
	}
	if (p1 > 0)
		count1 = 1e9;

	// Odd $50 and even $500 case.
	for (int i = 9; i >= 0; i--) 
	{
		if (money_num[i] == 0)
			continue;
		else if (i == 7 && p2 >= 500)	// Dealing with special case: $500.
		{
			if (p2 >= 1000 * change500_evennum)
			{
				count2 += change500_evennum * 2;
				p2 -= 1000 * change500_evennum;
			}
			else
			{
				count2 += (p2 / 1000) * 2;
				p2 %= 1000;
			}	
		}
		else if (i == 4 && p2 >= 50)	// Dealing with special case: $50.
		{
			if (p2 >= 100 * change50_oddnum)
			{
				count2 += change50_oddnum * 2;
				p2 -= 100 * change50_oddnum;
			}
			else
			{
				count2 += (p2 / 100) * 2;
				p2 %= 100;
			}	
		}
		else
		{
			if (p2 >= money[i] * money_num[i])
			{
				count2 += money_num[i];
				p2 -= money[i] * money_num[i];
			}
			else
			{
				count2 += p2 / money[i];
				p2 %= money[i];
			}	
		}
		if (p2 == 0)
			break;
	}
	if (p2 > 0)
		count2 = 1e9;

	// Even $50 and odd $500 case.
	for (int i = 9; i >= 0; i--) 
	{
		if (money_num[i] == 0)
			continue;
		else if (i == 7 && p3 >= 500)	// Dealing with special case: $500.
		{
			if (p3 >= 1000 * change500_oddnum)
			{
				count3 += change500_oddnum * 2;
				p3 -= 1000 * change500_oddnum;
			}
			else
			{
				count3 += (p3 / 1000) * 2;
				p3 %= 1000;
			}	
		}
		else if (i == 4 && p3 >= 50)	// Dealing with special case: $50.
		{
			if (p3 >= 100 * change50_evennum)
			{
				count3 += change50_evennum * 2;
				p3 -= 100 * change50_evennum;
			}
			else
			{
				count3 += (p3 / 100) * 2;
				p3 %= 100;
			}	
		}
		else
		{
			if (p3 >= money[i] * money_num[i])
			{
				count3 += money_num[i];
				p3 -= money[i] * money_num[i];
			}
			else
			{
				count3 += p3 / money[i];
				p3 %= money[i];
			}	
		}
		if (p3 == 0)
			break;
	}
	if (p3 > 0)
		count3 = 1e9;

	// Even $50 and even $500 case.
	for (int i = 9; i >= 0; i--) 
	{
		if (money_num[i] == 0)
			continue;
		else if (i == 7 && p4 >= 500)	// Dealing with special case: $500.
		{
			if (p4 >= 1000 * change500_evennum)
			{
				count4 += change500_evennum * 2;
				p4 -= 1000 * change500_evennum;
			}
			else
			{
				count4 += (p4 / 1000) * 2;
				p4 %= 1000;
			}	
		}
		else if (i == 4 && p4 >= 50)	// Dealing with special case: $50.
		{
			if (p4 >= 100 * change50_evennum)
			{
				count4 += change50_evennum * 2;
				p4 -= 100 * change50_evennum;
			}
			else
			{
				count4 += (p4 / 100) * 2;
				p4 %= 100;
			}	
		}
		else
		{
			if (p4 >= money[i] * money_num[i])
			{
				count4 += money_num[i];
				p4 -= money[i] * money_num[i];
			}
			else
			{
				count4 += p4 / money[i];
				p4 %= money[i];
			}	
		}
		if (p4 == 0)
			break;
	}
	if (p4 > 0)
		count4 = 1e9;

	// Find the minimal case.
	answer = min(count1, count2);
	answer = min(answer, count3);
	answer = min(answer, count4);
	if (answer == 1e9)
		printf("-1\n");
	else
		printf("%d\n", total_money_num - answer);
}

int main(void)
{
	int T;	// Number of test cases.
	int p;	// Target price.
	int total;	// Total money of input.

	scanf("%d", &T);
	while(T--)
	{
		scanf("%d", &p);
		for (int i = 0; i < 10; i++)
			scanf("%d", &money_num[i]);
		total = money_num[0] + 5 * money_num[1] + 10 * money_num[2] + 20 * money_num[3] + 50 * money_num[4] 
				+ 100 * money_num[5] + 200 * money_num[6] + 500 * money_num[7] + 1000 * money_num[8] + 2000 * money_num[9];
		total_money_num = money_num[0] + money_num[1] + money_num[2] + money_num[3] + money_num[4] 
					+ money_num[5] + money_num[6] + money_num[7] + money_num[8] + money_num[9];
		if (total < p)
			printf("-1\n");
		else
			beverage(total - p);
	}
	return 0;
}