#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

struct BlockInfo		// Structure used to store blocks and modified events.
{
	float index;
	unsigned long long value;
	int owner;
};

struct EventInfo		// Stucture used to store input events.
{
	int l;
	int r;
	unsigned long long v;
};

int T, n, m, q;		// T is the number of test cases; n, m and q is the number of players, gold blocks and events repectively. 

bool Compare(BlockInfo a, BlockInfo b)		// Compare function according to the index of blocks.
{
	return a.index < b.index;
}

void Scoring(int Left, int Right, vector< unsigned long long> &PlayerGoal, vector<unsigned long long> &PlayerScore, vector<int> &PlayerList, vector<EventInfo> &Event, vector<int> &Result, vector<vector<int> > &Owner, vector<unsigned long long> &V)		
// Recursive judging if players reach their goal.
{
	int i, j;
	int mid = (Left + Right) / 2;
	BlockInfo tmp;
	int score = 0;
	vector<BlockInfo> Storage;		// Used to store blocks and events and scoring.
	vector<int> Success;		// Stores players that succeed.
	vector<int> Failure;		// Stores players that fail.

	for (i = 0; i < PlayerList.size(); i++)		// Initialization.
	{
		V[PlayerList[i]] = 0;
	}
	for (i = 0; i < PlayerList.size(); i++) 		// Store the blocks which belong to players in the PlayerList.
	{
		for (j = 0; j < Owner[PlayerList[i]].size(); j++) {
			tmp.index = Owner[PlayerList[i]][j];
			tmp.value = 0;
			tmp.owner = PlayerList[i];
			Storage.push_back(tmp);
		}
	}
	for (i = Left; i <= mid; i++) 		// Modify events.
	{
		tmp.index = Event[i].l - 0.5;
		tmp.value = Event[i].v;
		tmp.owner = 0;
		Storage.push_back(tmp);
		tmp.index = Event[i].r + 0.5;
		tmp.value = -Event[i].v;
		tmp.owner = 0;
		Storage.push_back(tmp);
	}
	sort(Storage.begin(), Storage.end(), Compare);		// Sort the vector according to the index.

	for (i = 0; i < Storage.size(); i++) 		// Calculate the changing rate of score.
	{
		if (Storage[i].value != 0)	score += Storage[i].value;
		else	V[Storage[i].owner] += score;
	}
	for (i = 0; i < PlayerList.size(); i++) 		// Check if players reach their goal.
	{
		if (PlayerScore[PlayerList[i]] + V[PlayerList[i]] >= PlayerGoal[PlayerList[i]]) 
		{
			Result[PlayerList[i]] = mid;
			Success.push_back(PlayerList[i]);		// Success players: find backward.
		}
		else 
		{
			PlayerScore[PlayerList[i]] += V[PlayerList[i]];
			Failure.push_back(PlayerList[i]);		// Fail players: find forward.
		}
	}
	
	if (Left == Right)		// Terminate condition.
		return;
	if (Success.size() != 0)		// Recursion of success players.
		Scoring(Left, mid, PlayerGoal, PlayerScore, Success, Event, Result, Owner, V);
	if (Failure.size() != 0)		// Recursion of fail players.
		Scoring(mid + 1, Right, PlayerGoal, PlayerScore, Failure, Event, Result, Owner, V);
	return;
}

void Input(vector<unsigned long long> &PlayerGoal, vector<int> &PlayerList, vector<EventInfo> &Event, vector<vector<int> > &Owner)		// Scanning inputs.
{
	int i, j, k;
	BlockInfo b;
	EventInfo tmp;

	for (i = 1; i <= n; i++)		// Input each goal of players.
	{
		scanf("%lld", &PlayerGoal[i]);
		PlayerList.push_back(i);
	}
	for (j = 1; j <= m; j++)		// Input owners of each block.
	{
		scanf("%d", &b.owner);
		Owner[b.owner].push_back(j);
	}
	for (k = 1; k <= q; k++)		// Input events.
	{
		scanf("%d %d %lld", &Event[k].l, &Event[k].r, &Event[k].v);
	}
}

int main(void)
{
	int i, j;

	scanf("%d", &T);
	for (i = 0; i < T; i++)
	{
		scanf("%d %d %d", &n, &m, &q);

		vector<unsigned long long> PlayerGoal(n + 1);		// Stores the goal of each player.
		vector<unsigned long long> PlayerScore(n + 1);		// Stores the score of each player.
		vector<int> PlayerList(n + 1);		// Stores player.	
		vector<EventInfo> Event(q + 1);		// Stores events.
		vector<int> Result(n + 1, -1);		// Recording whether each player can reach the goal.
		vector<vector<int> > Owner(n + 1);		// Stores a index of which player owns which blocks.
		vector<unsigned long long> V(n + 1);		// Used to calculate scores.

		Input(PlayerGoal, PlayerList, Event, Owner);		// Input function.
		Scoring(1, q, PlayerGoal, PlayerScore, PlayerList, Event, Result, Owner, V);		// Scoring according to events.
		for (j = 1; j <= n; j++)		// Print answer.
		{
			if (j == n)	printf("%d\n", Result[j]);
			else	printf("%d ", Result[j]);
		}
	}
	return 0;
}