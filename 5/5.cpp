#include <cstdio>
#include <iostream>
#include <cstring>
using namespace std;
int tetriz[17][17];		// Storing blocking status.
int dp[17][17][65536];		// Storing different cases.
int power[] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536};		// 2 power table.
#define mod 1000000007
void fill_in_tetriz(int n, int m)
{
	int i, j, k;
	for (i = 1; i <= n; i++)
	{
		for (j = 0; j < m; j++)
		{
			for (k = 0; k <= power[m] - 1; k++)
			{
				if (i != 1 && j == 0)
				{
					dp[i][j][k] = dp[i - 1][m][k];
				}
				if ((tetriz[i][j] == 1) || ((k >> (m - 1)) == 1))		// 1 X 1 case or current block = 'X'.
				{
					dp[i][j + 1][((k - power[m - 1]) << 1) + 1] += dp[i][j][k];
					dp[i][j + 1][((k - power[m - 1]) << 1) + 1] %= mod;
				}
				if (((k >> (m - 1)) == 0) && (tetriz[i][j] == 0) && (tetriz[i - 1][j] == 0)) 		// Vertical 1 X 2 case.
				{
					dp[i][j + 1][(k << 1) + 1] += dp[i][j][k];
					dp[i][j + 1][(k << 1) + 1] %= mod; 
				}
				if (((k >> (m - 2)) == 3) && (tetriz[i][j] == 0) && (tetriz[i][j + 1] == 0) && (j <= m - 2))		// Horizontal 1 X 2 case.
				{
					dp[i][j + 2][(((k - power[m - 1]) - power[m - 2]) << 2) + 3] += dp[i][j][k];
					dp[i][j + 2][(((k - power[m - 1]) - power[m - 2]) << 2) + 3] %= mod; 
				}
				if (((k >> (m - 2)) == 0) && (tetriz[i][j] == 0) && (tetriz[i][j + 1] == 0) && (tetriz[i - 1][j] == 0) && (tetriz[i - 1][j + 1] == 0) && (j <= m - 2))		// 4 X 4 case.
				{	
					dp[i][j + 2][(k << 2) + 3] += dp[i][j][k];
					dp[i][j + 2][(k << 2) + 3] %= mod; 
				}
				if (((k >> (m - 1)) == 1) && tetriz[i][j] == 0)		// Not-fill-in case.
				{
					dp[i][j + 1][((k - power[m - 1]) << 1)] += dp[i][j][k];
					dp[i][j + 1][((k - power[m - 1]) << 1)] %= mod;
				}
			}
		}
	}
	printf("%d\n", dp[n][m][power[m] - 1] % mod);
}

int main(void)
{
	int T;		// Number of cases.
	int n, m, x;		// Rows and columns.

	scanf("%d", &T);
	for (int i = 0; i < T; i++)
	{
		scanf("%d %d", &n, &m);
		for (int j = 0; j <= n; j++)		// Set all default tetriz to be 1.
		{
			for (int k = 0; k <= m; k++)
			{
				tetriz[j][k] = 1;
			}
		}
		for (int j = 0; j < n; j++)
		{
			char input[17];		// Input string.
			scanf("%s", input);		// Input each line.
			for (int k = 0; k < strlen(input); k++)
			{
				if (input[k] == '.')
				{
					tetriz[j + 1][k] = 0;
				}
			}
		}
		for (int j = 0; j < 17; j++)		// Set all default dp to be 0.
		{
			for (int k = 0; k < 17; k++)
			{
				for (int x = 0; x < 65536; x++)
				{
					dp[j][k][x] = 0;
					if ((j == 1) && (k == 0))
					{
						dp[j][k][x] = 1;
					}
				}
			}
		}
		fill_in_tetriz(n, m);
	}
	return 0;
}