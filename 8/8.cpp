#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <queue>
using namespace std;

struct Block
{
	int step;	// Total steps so far;
	int beverage;	// Total beverages so far;
	char mark;	// Type of the cell.
};

struct Coordinate
{
	int x, y, z;	// Three directions.
};

Block cell[200][200][200];	// Stores each cell.
queue<Coordinate> q;

void Judge(int x, int y, int z, int step, int beverage)
{
	Coordinate temp;
	if (cell[x][y][z].mark != '#')	// Not a trap.
	{
		if (cell[x][y][z].step == -1)	// Haven't visit yet.
		{
			cell[x][y][z].step = step + 1;
			if (cell[x][y][z].mark == 'B')
			{
				cell[x][y][z].beverage = beverage + 1;
			}
			else
			{
				cell[x][y][z].beverage = beverage;
			}
			temp.x = x;
			temp.y = y;
			temp.z = z;
			q.push(temp);
		}
		else if (cell[x][y][z].step == step + 1)	// Visited.
		{
			if (cell[x][y][z].mark == 'B')
			{
				if (cell[x][y][z].beverage < beverage + 1)
				{
					cell[x][y][z].beverage = beverage + 1;
				}
			}
			else if (cell[x][y][z].beverage < beverage)
			{
				cell[x][y][z].beverage = beverage;
			}
		}
		else
		{
			if (cell[x][y][z].step > step)
			{
				cell[x][y][z].step = step;
				cell[x][y][z].beverage = beverage;	
			} 
		}
	}
}	

int main(void)
{
	int T;	// Test cases.
	int n;	// Size of grid.
	int i, j, k;

	scanf("%d", &T);	// Input test numbers.
	while (T--)
	{
		Coordinate Start;
		Coordinate Exit;
		scanf("%d", &n);	// Input the size of grid.
		for (i = 0; i <= n + 1; i++)
		{
			for (j = 0; j <= n + 1; j++)
			{
				for (k = 0; k <= n + 1; k++)
				{
					cell[i][j][k].step = -1;
					cell[i][j][k].beverage = 0;
					if (i == 0 || i == n + 1 || j == 0 || j == n + 1 || k == 0 || k == n + 1)
					{
						cell[i][j][k].mark = '#';	// Set bounds.
					}
					else
					{	
						scanf("%c", &cell[i][j][k].mark);	// Input cell.
					}
					if (cell[i][j][k].mark == '\n')	// Check for '\n';
					{
						scanf("%c", &cell[i][j][k].mark);
					}
					if (cell[i][j][k].mark == 'S')	// Start.
					{
						cell[i][j][k].step = 0;
						Start.x = i;
						Start.y = j;
						Start.z = k;
					}
					else if (cell[i][j][k].mark == 'E')	// Exit.
					{
						Exit.x = i;
						Exit.y = j;
						Exit.z = k;
					}
				}
			}
		}
		q.push(Start);
		while (!q.empty())	// Push each block into the queue.
		{
			Coordinate cur = q.front();
			Judge(cur.x + 1, cur.y, cur.z, cell[cur.x][cur.y][cur.z].step, cell[cur.x][cur.y][cur.z].beverage);
			Judge(cur.x - 1, cur.y, cur.z, cell[cur.x][cur.y][cur.z].step, cell[cur.x][cur.y][cur.z].beverage);
			Judge(cur.x, cur.y + 1, cur.z, cell[cur.x][cur.y][cur.z].step, cell[cur.x][cur.y][cur.z].beverage);
			Judge(cur.x, cur.y - 1, cur.z, cell[cur.x][cur.y][cur.z].step, cell[cur.x][cur.y][cur.z].beverage);
			Judge(cur.x, cur.y, cur.z + 1, cell[cur.x][cur.y][cur.z].step, cell[cur.x][cur.y][cur.z].beverage);
			Judge(cur.x, cur.y, cur.z - 1, cell[cur.x][cur.y][cur.z].step, cell[cur.x][cur.y][cur.z].beverage);
			q.pop();
		}
		if (cell[Exit.x][Exit.y][Exit.z].step == -1)
		{
			printf("Fail OAQ\n");
		}
		else
		{
			printf("%d %d\n", cell[Exit.x][Exit.y][Exit.z].step, cell[Exit.x][Exit.y][Exit.z].beverage);
		}
	}
	return 0;
}